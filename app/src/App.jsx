import React , { Component }from 'react'
import Popup from 'react-popup';

import InvestorPanel from './views/investorPanel/InvestorPanel'
import Counter from './views/Counter'
import HeaderInvestor from './views/HeaderInvestor'
import TotalSupply from './views/TotalSupply'
import BuyTokens from './views/BuyTokens'
import './stylesheets/popup.css'
import Network from './network'
import { skaraTokenAddress, skaraCrowdsaleAddress } from './contracts'

export default class App extends Component {

  constructor (){
    super()

    var url_string =  window.location.href
    var url = new URL(url_string);
    var isLite = url.searchParams.get("lite");
    console.log("isLite", isLite);

    this.state = {
      address: null,
      isLite:isLite
    }
  }

  render() {
    const {isLite} = this.state;

    return  (
      <div>
        {!isLite ? <HeaderInvestor onValidAddress = {this.login} logout={this.logout}/> : null}
        
        {this.state.address 
         ? 
         <div>
           <BuyTokens/>
           <InvestorPanel  address={ this.state.address } logout={this.logout}/>
         </div>
         :
         <div>
          <Counter/>
          <TotalSupply/>
         
          {!isLite ?  <BuyTokens/>: null}
         </div>
        }
        
      </div>
    )
   
  }

  login = (address) => {
    this.setState({address});
  }
  logout = (address) => {
    this.setState({address:null});
  }
}

