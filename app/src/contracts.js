import contract from 'truffle-contract'
import Network from "./network"

import vestingJson from './contracts/TokenVesting.json'
import tokenJson from './contracts/SimpleToken.json'

import skaraCrowdsaleJson from './contracts/skara/SkaraCrowdsale.json'
import skaraTokenJson from './contracts/skara/SkaraToken.json'

export async function getTokenVesting(address) {
  const TokenVesting = contract(vestingJson)
  const provider = await Network.provider()
  TokenVesting.setProvider(provider)
  return TokenVesting.at(address)
}

export async function getSimpleToken(address) {
  const SimpleToken = contract(tokenJson)
  const provider = await Network.provider()
  SimpleToken.setProvider(provider)
  return SimpleToken.at(address)
}

export async function skaraTokenTotalSupply(callback){
 
  const eth = await Network.eth();
  if(Network.web3Unavailable){
    callback('NO_WEB3', null)
    return;
  } 
  const SkaraToken = eth.contract(skaraTokenJson.abi);
  const skaraTokenInstance = SkaraToken.at(skaraTokenAddress());

  skaraTokenInstance.totalSupply(callback);
}

export function skaraTokenMaxSupply() {
  return  isPresale() ? 7408053 : 12348423;
}

export function skaraTokenHardCap() {
  return  10000000;
}

export function isPresale() {
  return Date.now() < presaleEnd() 
}

export function isSale() {
  return Date.now() > presaleEnd() && Date.now() < saleEnd() 
}

export function saleEnd() {
  return 1519398000000 
}

export function presaleEnd() {
  return 1518102000000 
}

export function skaraTokenAddress() {
  //return "0x409ba3dd291bb5d48d5b4404f5efa207441f6cba"; //dev
  return "0x512ad4de9121f8ea18c7081f57eabff2fdc73a28"; //live
  //return "0x793b1adf46a0678d49c6a0ef4efe1e8a740e04a7"; //kovan
}
export function skaraCrowdsaleAddress() {
  //return "0x2c2b9c9a4a25e24b174f26114e8926a9f2128fe4";  //dev
  return "0x912b9312ee121a21f6ee347faa6d5f639a75fc8e";  //live
  //return "0xdf58af6cbfb5d0ba66dc8ac40a4e1beffe19024e";  //kovan
}

export function skaraCrowdsaleInitialBlock() {
  return 4965489; //live
  //return 5672533;
}

export async function getVestingContract(beneficiary, callback) {
  const eth = await Network.eth();
  if(Network.web3Unavailable){
    callback(null, 'NO_WEB3')
    return;
  } 
  const SkaraCrowdsale = eth.contract(skaraCrowdsaleJson.abi);
  const skaraCrowdsaleInstance = SkaraCrowdsale.at(skaraCrowdsaleAddress());
  
  skaraCrowdsaleInstance.TokenVestingCreated({'beneficiary': beneficiary}, { fromBlock: skaraCrowdsaleInitialBlock(), toBlock: 'latest' })
    .get((error, eventResult) => {
        if (error || eventResult.length == 0){
          callback(null, 'NOT_FOUND')
        }
        else
        {
          var found = eventResult.filter((e) => {return e.args.beneficiary.indexOf(beneficiary)  !== -1})
          if(!found)
          {
            callback(null, 'NOT_FOUND')
          }
          else{
            callback(found, null)
          }
        
        }
      });
}


export async function getTokenPurchases(beneficiary) {

  const eth = await Network.eth();
  const SkaraCrowdsale = eth.contract(skaraCrowdsaleJson.abi);
  const skaraCrowdsaleInstance = SkaraCrowdsale.at(skaraCrowdsaleAddress());

  return new Promise((resolve, reject) => {
    skaraCrowdsaleInstance.TokenPurchase({'beneficiary': beneficiary}, { fromBlock: skaraCrowdsaleInitialBlock(), toBlock: 'latest' })
    .get((error, eventResult) => {
        if (error || eventResult.length == 0){
          resolve(null, null)
        }
        else
        {
          resolve(eventResult, null)
        }
      });
  })
}

export async function getPostsalerAmount(beneficiary) {
  
    const eth = await Network.eth();
    const SkaraCrowdsale = eth.contract(skaraCrowdsaleJson.abi);
    const skaraCrowdsaleInstance = SkaraCrowdsale.at(skaraCrowdsaleAddress());
  
    return new Promise((resolve, reject) => {
      skaraCrowdsaleInstance.getPostsalerAmount.call(beneficiary,{from: beneficiary},
          (error, result) => {
            if (error){
              resolve(0,null)
            }
            else
            {
              resolve(result, null)
            }
          }
      )
    })
  }

  export async function claimFromPostsaler(beneficiary) {
    
      /*
      const eth = await Network.eth();
      const SkaraCrowdsale = eth.contract(skaraCrowdsaleJson.abi);
      const skaraCrowdsaleInstance = SkaraCrowdsale.at(skaraCrowdsaleAddress());
    
      var claimFn = skaraCrowdsaleInstance.claimFromPostsaler(beneficiary,{from: beneficiary});

      eth.sendTransaction({to:skaraCrowdsaleAddress(), from:beneficiary, data: claimFn})

      return new Promise((resolve, reject) => {
        skaraCrowdsaleInstance.claimFromPostsaler.call(beneficiary,{from: beneficiary},
            (error, result) => {
              if (error){
                reject(error)
              }
              else
              {
                resolve(result, null)
              }
            }
        )
      })
      */
    }