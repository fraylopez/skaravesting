import Web3 from 'web3'
import { sleep } from './utils'


const Network = {
  web3Unavailable : false,

  async web3() {
    if(Network.web3Unavailable) return null;

    const provider = await Network.provider()
    return new Web3(provider)
  },

  async eth() {
    if(Network.web3Unavailable) return null;
    const web3 = await Network.web3()
    return web3.eth
  },

  async provider() {
    if(Network.web3Unavailable) return null;

    let { web3 } = window
    //const web3p = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/ '))
    
    var wait = 0;
    while (web3 === undefined && wait < 10000) {
      await sleep(500)
      wait += 500;
      web3 = window.web3
      /*wweb3 = window.web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/'))
      eb3 = window.web3 = new Web3(
        ZeroClientProvider({
          static: {
            eth_syncing: false,
            web3_clientVersion: 'ZeroClientProvider',
          },
          pollingInterval: 99999999, // not interested in polling for new blocks
          rpcUrl: 'https://mainnet.infura.io/',
          // account mgmt
          getAccounts: (cb) => cb(null, [])

        })
      );
      */
    }
    if(web3) return web3.currentProvider;

    Network.web3Unavailable = true;
    return null;
  },

  getAccounts() {
    if(Network.web3Unavailable) return null;

    return new Promise((resolve, reject) => {
      Network.eth().then(eth => eth.getAccounts(Network._web3Callback(resolve, reject)))
    })
  },

  _web3Callback(resolve, reject) {
    return (error, value) => {
      if (error) reject(error)
      else resolve(value)
    }
  },

  log(msg) {
    console.log(`[Network] ${msg}`)
  },

  blockToTimestamp(block) {
    if(Network.web3Unavailable) return null;
    return new Promise((resolve, reject) => {
      Network.eth().then(eth => eth.getBlock(block, Network._web3Callback(resolve, reject)))
    })
  }

  
}

export default Network