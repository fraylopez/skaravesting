import React, { Component }from 'react'
import { Grid, Col, Row } from 'react-bootstrap'
import Clipboard from 'clipboard'

import { ContractLink, TokenLink } from './Links'
import { skaraTokenAddress, skaraCrowdsaleAddress } from './../contracts'
import Network from './../network'
import Popup from 'react-popup';
import './../stylesheets/buy-tokens.css'

export default class BuyTokens extends Component
{
  constructor(){
    super()
    this.state = {ethInSKRT:0}
    new Clipboard('.copy-address-btn');
  }

  render() {

    return ( 
      <div className="buy-tokens section">
        <Popup/>
   
        <div className="info">
          <div className="buy-address">
            <p>Skara Crowdsale Contract:</p>
            <p className="skara-address"><span>{skaraCrowdsaleAddress()}</span><button className={"copy-address-btn" + (this.state.addressCopied ? " copied" : "")} onClick={()=>this.setState({addressCopied:true})} data-clipboard-text={skaraCrowdsaleAddress()}>{this.state.addressCopied ? "COPIED" : "COPY"}</button></p>
          </div>
          {
            !this.state.web3Unavailable ?
              <div className="buy-form">
                <p>Your investment (ETH):</p>
                <input  ref={(i) => this.investmentInput = i} onChange={this.onInputChange}></input>
                <button onClick={this.generateTransaction} disabled={this.state.web3Unavailable}>BUY TOKENS</button>
                {this.state.ethInSKRT ? <p>{this.state.ethInSKRT} SKRT</p> : null}
              </div>
            :
            <p>Ethereum Network is not available...</p>
          }
        </div>
      
      <div className="qrcode">
        <img  src={process.env.PUBLIC_URL + "/qrcode.png"}  />
      </div>
            
          
    </div>
    )
    
  }

  copyAddressToClipboard = () => {
    new Clipboard(".skara.address")
  }

  onInputChange = () => {

    this.setState({ethInSKRT: this.investmentInput.value*1000})
  }

  generateTransaction = async () => {
    const eth = await Network.eth();
    if(!eth){
      this.setState({web3Unavailable: true})
      return
    }

    const accounts = await Network.getAccounts()
    //console.log(window.investmentInput.value)
    if(accounts.length > 0){
      eth.sendTransaction(
        {
          from:accounts[0],
          to:skaraCrowdsaleAddress(),
          value: this.investmentInput.value*1000000000000000000,
          gas: 1000000,
          gasPrice:20e9},
          (error, result) =>{
            //console.log(result);
            this.forceUpdate()
          })
    }
    else{
      Popup.clearQueue();
      Popup.alert(<span>Please, enable <a href="https://metamask.io/" target="_blank">Metamask</a> or any other wallet provider</span>)
    }
  }

 
  

}

