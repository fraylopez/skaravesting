import React, { Component } from 'react'
import Countdown from 'react-countdown-now';

import {isPresale, isSale, presaleEnd, saleEnd } from './../contracts'

import './../stylesheets/counter.css'


export default class Counter extends Component {
  constructor() {
    super()
    this.state = {showHelp:false, loading: false, error:""};
  }


  render() {
    const { address, token } = this.props
      return (
      <div className="counter section">
        {isPresale() ? <p className="sale-end">Presale ends in:</p> : <p className="sale-end">Sale ends in:</p>}
        <Countdown date={this.getCoundountOffset() } renderer={this.renderer}/>
      </div>
    )
    
  }

   
  // Renderer callback with condition
  renderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
      // Render a completed state
      return <span>Skara Token sale is finished!</span>;
    } else {
      // Render a countdown
      return(
        <table>
          <tr>
            <th className="counter-num">{days}</th>
            <th className="counter-num"></th>
            <th className="counter-num">{hours}</th>
            <th className="counter-num"></th>
            <th className="counter-num">{minutes}</th>
            <th className="counter-num"></th>
            <th className="counter-num">{seconds}</th>
          </tr>
          <tr>
            <th className="counter-unit">days</th>
            <th className="counter-unit"></th>
            <th className="counter-unit">hours</th>
            <th className="counter-unit"></th>
            <th className="counter-unit">minutes</th>
            <th className="counter-unit"></th>
            <th className="counter-unit">seconds</th>
          </tr>
        </table>
      )
       
    }
  };

  getCoundountOffset = ()=>{
    if(isPresale()) return presaleEnd()
    if(isSale()) return saleEnd()

    return 0;
  }

}


