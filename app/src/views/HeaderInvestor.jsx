import React, { Component }from 'react'
import { Grid, Col } from 'react-bootstrap'
import { ContractLink, TokenLink } from './Links'
import Popup from 'react-popup';

import LoginForm from './LoginForm'
import './../stylesheets/header-investor.css'


export default class HeaderInvestor extends Component
{
  constructor(){
    super()
    this.state={logged:false};
  }

  render() {
    return ( 
      <header className="header-investor">

          <Popup/>
          <div className="header-left">
            <a target="_blank" href="https://www.skaratoken.com" rel="noopener noreferrer">
              <img className="header-logo" src={process.env.PUBLIC_URL + "/logo-skara.png"} alt="Skara logo" />
            </a>
          </div>
          <div className="header-center">
            <h3>Investor's Panel</h3>
          </div>
          
          <div className="header-right">
            {!this.state.logged ? 
              <button className="login-btn" onClick={() => this.handleLogin()}>LOGIN</button> 
              : 
              <div className="logged">
                <p>Logged as:</p>
                <p>{this.state.address}</p>
                <a className="logout" onClick={this.logout}>Logout</a>
              </div>
              }
          </div>
      </header>
    )
  }

  handleLogin = () => {
    let popup = Popup.register({
      title: 'Login',
      content: <LoginForm onValidAddress = {this.onValidAddress}/>,
      
    })

    Popup.clearQueue();
    Popup.queue(popup);
  }

  onValidAddress = (address) => {
    Popup.close();

    this.setState({logged:true, address:address});
    this.props.onValidAddress(address);
  }

  logout = () => {
    this.setState({logged:false, address:null});
    this.props.logout();
  }
}




/*
class LoginForm extends Component {
  constructor() {
    super()
    this.state = {showHelp:false, loading: false, error:""};
  }
  
  render(){
    return (
      <div className="login-form-content">
        <p>Yout wallet address</p>
        <input ref={(c) => this.loginInput = c}/>
      </div>
    )
    
  }
}
*/