import React, { Component } from 'react'

class Help extends Component {
  constructor() {
    super()
    this.state = { name: 'Token', loading: false }
  }

  componentDidMount() {
    //this.getData()
  }

  render() {
    const { address, token } = this.props
    return (
      <div className="help-content">
        <div className="centered">
          <p>Follow this simple steps: </p>
          <p>1. Go to your transaction details page in <a  target="_blank" href="https://etherscan.io">Etherscan</a>. You most likely will find a link in your transaction app (i.e. Metamask)</p>
          <p>2. Click on the token holder link</p>
          <p>3. Copy the token holder address on the form above</p>
          <img  className="" src={process.env.PUBLIC_URL + "/vestingviewer-1.png"} alt="Skara logo" />
          <img  className="" src={process.env.PUBLIC_URL + "/vestingviewer-2.png"} alt="Skara logo" />

        </div>
      </div>
    )
  }
 
}


export default Help
