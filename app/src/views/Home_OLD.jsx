import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import Network from "./../network"
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { getTokenVesting, getSimpleToken, skaraTokenAddress } from '../contracts'

import Header from './Header'
import VestingDetails from './VestingDetails'
import VestingSchedule from './VestingSchedule'
import Spinner from './Spinner'
import Help from './Help'

import '../stylesheets/TokenVestingApp.css'


class Home extends Component {
  constructor() {
    super()
    this.state = {showHelp:false};
    console.log("HOME")
  }


  render() {
    const { address, token } = this.props
    return (
      <div className="">
        <div className="header-home">
          <a className="" target="_blank" href="https://www.skaratoken.com" rel="noopener noreferrer">
            <img  className="" src={process.env.PUBLIC_URL + "/logo-skara.png"} alt="Skara logo" />
            <h1 className="">Token Vesting Viewer</h1>
          </a>
        </div> 
        <div className="form">
        <p className="welcome-text centered">Thank you for contributing to Skara Token sale! You can check your token vesting schedule using this form. Use the link below if you require assistance for finding your token holder address.</p>
          <div>
            <span>Token holder address:</span> <input className="inputField" ref={(c) => window.inputField = c} type="text" onKeyPress={this.handleKeyPress} name="vestingcontract"/>
            <button className="go-button" onClick={this.redirectToContract.bind(this)}>Go!</button>
            <a className="help-link" onClick={() => this.setHelp(!this.state.showHelp)}>Find your token holder address</a>
          </div>
        </div>
        { this.state.showHelp ? <Help /> : null }
      </div>
    )
  }
     
  setHelp(showHelp) {
    this.setState({ showHelp })
  }
  

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.redirectToContract(window.inputField.value)
    }
  }

  redirectToContract = () => {
    //window.location.href = process.env.PUBLIC_URL + '/' + address + '/' + skaraTokenAddress();
    const address = window.inputField.value;
    this.props.history.push(address + '/' + skaraTokenAddress()); 
  }

}


export default Home
