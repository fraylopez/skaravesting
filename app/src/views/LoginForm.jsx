import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import Network from "./../network"
import { 
  skaraCrowdsaleAddress, 
  skaraCrowdsaleAbi, 
  getTokenVesting, 
  getSimpleToken,
  skaraTokenAddress } from "./../contracts"

import Help from './Help'



export default class LoginForm extends Component {
  constructor() {
    super()
    this.state = {showHelp:false, loading: false, error:""};
  }


  render() {
    const { address, token } = this.props
      return (
      <div className="home">
        { 
          this.state.error ? 
          <div className="error">
            <p className="error-text">Something went wrong, please <a onClick={() => this.setState({error: ""})}>try again</a></p>
            <p className="error-trace">{this.state.error}</p>
          </div>
          :  
          (
            <div className="">
              <div className="form">
              <p className="welcome-text centered">Welcome to Skara's Token Sale, please install <a href="https://metamask.io/" target="_blank" >Metamask</a> extension and input your wallet address to begin.</p>
                <div>
                  <span>Wallet address:</span> <input className="inputField" ref={(c) => this.walletInputField = c} type="text" onKeyPress={this.handleKeyPress} name="vestingcontract"/>
                  <button className="go-button" onClick={() => this.handleAddress()}>Go!</button>
                </div>
                {this.state.wrongAddress ? <p className="wrong-address">This address seems invalid. Please double-check it.</p> : null}

              </div>
            </div>
          )
        
      }
       { this.state.showHelp ? <Help /> : null }
      </div>
    )
    
  }
     
  handleAddress = () => {
    this.setState({wrongAddress:false})
    var address = this.walletInputField.value.toLowerCase();
    if(address && address.length >= 40 && address.length <= 42 ){
      this.props.onValidAddress(address)
    }
    else{
      this.setState({wrongAddress:true})
    }
  }

  setHelp(showHelp) {
    this.setState({ showHelp })
  }

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.handleAddress()
    }
  }
}


