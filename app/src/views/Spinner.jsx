import React from 'react'

function Spinner() {
  return <div className="spinner">
    <img src={process.env.PUBLIC_URL + "/spinner.gif"} alt="spinner" />
    {/*<TipWeb3Provider />*/}
  </div>
}

const TipWeb3Provider = () => (
  <p>Please, be patient...</p>
)

export default Spinner