import React, { Component } from 'react'
import Network from "./../network"
import { skaraTokenAddress, skaraTokenAbi, skaraTokenMaxSupply, skaraTokenTotalSupply, skaraTokenHardCap } from "./../contracts"
import Popup from 'react-popup';

import Spinner from './Spinner'
import {BigNumber} from 'bignumber.js';
import { Progress } from 'react-sweet-progress';
import "react-sweet-progress/lib/style.css";

import './../stylesheets/total-supply.css'

//import ProgressBar from 'react-progress-bar-component';
//import '../stylesheets/progressbar.css'

class TotalSupply extends Component {
  constructor() {
    super()

    var url_string =  window.location.href
    var url = new URL(url_string);
    var isLite = url.searchParams.get("lite");

    this.state = 
    {
      isLite:isLite,
      totalSupply : 0,
      loading: false, // set to true if loading starts on mount
      completeLoading: false
    }
  }

  componentWillMount = () => {
    this.setState({loading:true})
    skaraTokenTotalSupply((error, result) => {
      if(!error){
        const SKRTExpMinus18 = result;
        const SKRT = SKRTExpMinus18.div(new BigNumber(1e18))
        this.setState(
          {
            totalSupply:SKRT,
            totalSupplyString:SKRT.toFixed(0) 
          })
      }
      if(error == 'NO_WEB3'){
        Popup.alert(<p>It seems that you don't have access to ethereum. Please install <a href="https://metamask.io/" target="_blank" >Metamask</a> extension and try again.</p>)
        this.setState({noWeb3:true})
      }
      this.setState({loading:false})
    });

      
 
  }

  getDataObj(to, func, arrVals) {
      var val = "";
      for (var i = 0; i < arrVals.length; i++) val += this.padLeft(arrVals[i], 64);
      return {
          to: to,
          data: func + val
      };
  }

  render() {
      //const currentPercentage = (100*this.state.totalSupply/skaraTokenTotalSupply()).toFixed(2);
      const currentPercentage = (100*this.state.totalSupply/skaraTokenHardCap()).toFixed(2);
      return (
      <div className={"total-supply section"}>
        
        {!this.state.noWeb3 ? 
          <div>
            <p className="total-supply-distributed"><img src={process.env.PUBLIC_URL + "/favicon.png"}/>SKRT tokens distributed:<span className="token-number">{this.state.loading ? <img src={process.env.PUBLIC_URL + "/spinner.gif"}/> : null}{this.state.totalSupplyString}</span></p>
            <div className="supply-graphs">
              <div className="soft-graph graph">
                <div className="of-max-supply">
                  {/*<p className="success">SUCCESS!</p>*/}
                  <p className="of-max-supply-text">Soft cap - 2m tokens</p>
                </div>
                <Progress 
                  type="circle"
                  strokeWidth={10}
                  percent={100} 
                  theme={{
                    success:  {
                      color: '#cf9a34',
                      symbol: <span className="inner-percentage">100%</span>
                      
                    }
                }}/>
              </div>
              <div className="hard-graph graph">
                <div className="of-max-supply">
                  <p className="of-max-supply-text">Hard cap - 10m tokens</p>
                </div>
                <Progress 
                  type="circle"
                  strokeWidth={10}
                  percent={currentPercentage}
                  theme={{
                    symbol: this.state.loading ? <img src={process.env.PUBLIC_URL + "/spinner.gif"}/> : <span className="inner-percentage">{currentPercentage}</span> 
                  }}
                />
              </div>
            </div>
            {!this.state.isLite ? <p className="success-text"><span className="success">SUCCESS!</span> SKRT will be available in-game on Skara Beta Release - 15th Feb</p> : null }

          </div>
          :
          <p>It seems that you don't have access to ethereum. Please install <a href="https://metamask.io/" target="_blank" >Metamask</a> extension and try again.</p>
        }
        
        
      </div>
    )
    
  }

  startLoading = () => {
    this.setState({startLoading: true});
  }

  endLoading = () => {
    this.setState({completeLoading: true});
  }

	resetLoading = () => {
    this.setState({
      startLoading: false,
      completeLoading: false
    });
  }

}


export default TotalSupply
