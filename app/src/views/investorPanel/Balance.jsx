import React, { Component } from 'react'
import './../../stylesheets/investor-panel.css'
import { Progress } from 'react-sweet-progress';
import Spinner from './../Spinner'
import TokenVestingApp from './TokenVestingApp'
import { 
   skaraTokenAddress,
   skaraCrowdsaleAddress,
   getVestingContract,
   getTokenVesting, 
   getSimpleToken, 
   skaraTokenMaxSupply, 
   skaraTokenTotalSupply,
   getPostsalerAmount
  } from "./../../contracts"
import Network from './../../network'
import { displayAmount } from '../../utils'
import {BigNumber} from 'bignumber.js';
import skaraCrowdsaleJson from './../../contracts/skara/SkaraCrowdsale.json'

import { Table,  Grid, Row, Col } from 'react-bootstrap'
import Popup from 'react-popup';

export default class Balance extends Component {
  constructor() {
    super()
    this.state = { balance: 0, loading :true }
  }
  
  componentDidMount =  () => {
    this.getBalance();
    skaraTokenTotalSupply((error, result) => {
      if(!error){
        const SKRTExpMinus18 = result;
        const SKRT = SKRTExpMinus18/(1e18)
        this.setState(
          {
            claimable:false,
            totalSupply:SKRT,
          })
        }
      });
  }

  getBalance = async () => {
    this.setLoader(true);
    const { address, vestings } = this.props;

    const tokenContract = await getSimpleToken(skaraTokenAddress());
    const userBalance  = await tokenContract.balanceOf(address);
    const postSaleBalance = await getPostsalerAmount(address);

    var total = userBalance.plus(postSaleBalance);
    var totalVesting = 0;
    for (var i = 0; i < vestings.length; i++) {
      const vestingAddress = vestings[i].args.vestingContract;
      const tokenVesting = vestingAddress ? await getTokenVesting(vestingAddress) : null
      const vestingBalance  = await tokenContract.balanceOf(vestingAddress)
      const released = tokenVesting ? await tokenVesting.released(skaraTokenAddress()) : 0;
  
      totalVesting = vestingBalance.plus(released)
      total = total.plus(totalVesting);
    }
    const claimableAmount = postSaleBalance != totalVesting;
    
    this.setState({claimable:claimableAmount, balance:total});
    this.setLoader(false);
  } 


  formatTokens = (amount) => {
    if (amount == null) return
    const display = displayAmount(amount, 18)

    return `${display} ${"SKRT"}`
  }


  render() {
    const { balance} = this.state
    return (
      <div className="balance">

        { this.state.loading ? <Spinner /> : null }
        <h4>Investor detalis</h4>
        <Grid>
            <Row>
              <Col xs={12} md={6}>
                <Table striped bordered condensed>
                  <tbody>
                  
                    <TableRow title="Balance">
                      { this.formatTokens(balance) }
                    </TableRow>
                    
                    <TableRow title="Percent of total">
                      { (this.state.totalSupply ? 100*displayAmount(balance,18)/this.state.totalSupply : 0 ).toFixed(2) + " %"}
                    </TableRow>
                    
                    {this.state.claimable ? 
                      <TableRow title="Claim">
                        <button onClick={this.claim}>CLAIM</button>
                      </TableRow>
                      :
                      null
                    }
                  </tbody>
                </Table>
              </Col>

              <Col xs={12} md={6}>
              { 
                <Progress
                  type="circle"
                  strokeWidth={5}
                  percent={(this.state.totalSupply ? 100*displayAmount(balance,18)/this.state.totalSupply : 0 ).toFixed(2) }
                />
              }
              </Col>
            </Row>
          
          </Grid>
        
      </div>
         
    )
  }
 
  claim = async () => {
    const eth = await Network.eth();
    if(!eth){
      this.setState({web3Unavailable: true})
      return
    }

    const accounts = await Network.getAccounts()
    //console.log(window.investmentInput.value)
    if(accounts.length > 0){
      const SkaraCrowdsale = eth.contract(skaraCrowdsaleJson.abi);
      const skaraCrowdsaleInstance = SkaraCrowdsale.at(skaraCrowdsaleAddress());
      var claimFn = skaraCrowdsaleInstance.claimFromPostsaler.getData(this.props.address);
      eth.sendTransaction(
        {
          from:accounts[0],
          to:skaraCrowdsaleAddress(),
          data: claimFn,
          gas: 1e6,
          gasPrice:20e9
        },
        (error, result) =>{
          //console.log(result);
          this.forceUpdate()
        })
    }
    else{
      Popup.clearQueue();
      Popup.alert(<span>Please, enable <a href="https://metamask.io/" target="_blank">Metamask</a> or any other wallet provider</span>)
    }
  }
  setLoader= (loading) => {
    this.setState({ loading })
  }

}


function TableRow({ title, children }) {
  return (
    <tr>
      <th>{ title }</th>
      <td>
        { children }
      </td>
    </tr>
  )
}
