import React, { Component } from 'react'
import Popup from 'react-popup';

import './../../stylesheets/investor-panel.css'
import { Progress } from 'react-sweet-progress';
import Spinner from './../Spinner'
import {  
  skaraTokenAddress, 
  getVestingContract, 
  getTokenVesting, 
  getSimpleToken , 
  getTokenPurchases} from "./../../contracts"

import {displayAmount, sleep} from "./../../utils"
import Network from "./../../network"

import TokenVestingApp from './TokenVestingApp'
import Balance from './Balance'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

export default class InvestorPanel extends Component {
  constructor() {
    super()
    this.state = { 
      name: 'loading...', 
      loading: true ,
      vestingAddress: null,
      vestings:[],
      walletNotFound:false
    }
  }
  
  componentDidMount =  () => {
    this.getData();
  }

  getData = async () => {
    this.setLoader(true);
    const { address } = this.props;
    getVestingContract(address, (vestings, error) =>{
      this.setLoader(false);
      if(!error && vestings.length > 0){
        this.setState({vestings})
       
      }
      else{
        if(error == 'NO_WEB3'){
          Popup.alert(<p>It seems that you don't have access to ethereum. Please install <a href="https://metamask.io/" target="_blank" >Metamask</a> extension and try again.</p>)
        }
        /*else if(!vestings || vestings.length == 0) {
          this.setState({walletNotFound:true})
          Popup.alert("Wallet not found. Please logout, double-check the address and try again.")
        }*/
      }
    })
    
  }
  render() {
    const { address} = this.props;
    const { vestingAddress} = this.state;

    return (
      <div className="investor-panel">
        {this.state.walletNotFound ? <p className="wallet-not-found">Wallet not found. Please logout, double-check the address and try again.</p> :
        this.state.loading ? <Spinner /> : 
          (
            <div className="investor-panel">
              <Balance address={ address} vestings={ this.state.vestings }/>
              <History address={ address} vestings={this.state.vestings} showVesting={this.showVesting}/> 
              {vestingAddress ? 
                <TokenVestingApp address={vestingAddress} token={skaraTokenAddress()}/>
                : 
                null}
            </div>
          )
        
      }
                
      </div>
    )
  }

  showVesting = async(vestingAddress) => {
    this.setState({vestingAddress:null})
    await sleep(500); 
    this.setState({vestingAddress})
  }
  setLoader= (loading) => {
    this.setState({ loading })
  }

}

class History extends Component {
  constructor() {
    super()
    this.state = {purchases:null, loading:false}
  }
  componentDidMount =  () => {
    this.getData();
    
  }

  getData = async () => {

    this.setState({loading:true})
    const beneficiary = this.props.address;
    const purchasesData = await getTokenPurchases(beneficiary);
    if(purchasesData){
      const purchases = await this.processPurchasesData(purchasesData);
      this.parseData(purchases);
    }
    else{
      this.setState({loading:false})
    }
    
   
  }

  processPurchasesData = async (purchasesData) => {
    var purchases = []
    for (var i = 0; i < purchasesData.length; i++){
      const purchaseData = purchasesData[i];
      const block = await Network.blockToTimestamp(purchaseData.blockNumber)
      const purchaseParsedData = {
        transactionHash: purchaseData.transactionHash,
        date: new Date(block.timestamp*1000).toLocaleString(),
        eth:  displayAmount(purchaseData.args.value,18),
        skrt: displayAmount(purchaseData.args.amount,18),
      }
      purchases.push(purchaseParsedData);
    }
    return purchases;
  }

  parseData = (purchases) => {
    const {vestings} = this.props;

    purchases.forEach((purchase, index) => {
      purchase['bonus'] = (100*purchase.skrt/(purchase.eth*1000) - 100).toFixed(1) + " %"
      vestings.forEach((vesting, index) => {
        if(purchase.transactionHash == vesting.transactionHash)
        {
          purchase['vesting'] = vesting.args.vestingContract
        }
      })
    })
   
    this.setState({purchases:purchases, loading:false})
    
    
  }
  
  render(){
    
    return(
      <div>
          {this.state.loading ? <Spinner/> : null}
          {this.state.purchases ?
              <div className="history">
                <BootstrapTable data={this.state.purchases}>
                  <TableHeaderColumn dataField='date'>Date</TableHeaderColumn>
                  <TableHeaderColumn dataField='transactionHash' dataFormat={ this.transactionRenderer } isKey >Tx Hash</TableHeaderColumn>
                  <TableHeaderColumn dataField='eth' >ETH</TableHeaderColumn>
                  <TableHeaderColumn dataField='skrt' >SKRT</TableHeaderColumn>
                  <TableHeaderColumn dataField='bonus' >Bonus</TableHeaderColumn>
                  <TableHeaderColumn dataField='vesting' dataFormat={ this.vestingRenderer }>Vesting</TableHeaderColumn>
                </BootstrapTable>
              </div>
              :
            null}
          
      </div>

        
          
        
      
    )
  }

  transactionRenderer = (cell, row) => {
    return (
      <a href={"https://etherscan.io/tx/" +cell} target="_blank">{cell}</a>
    )
  }

  vestingRenderer = (cell, row) => {
    return (
      cell ? <button onClick={()=> this.props.showVesting(cell)}>GO</button> : null
    )
  }
  
}


